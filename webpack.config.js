const path = require('path');
const pkg = require('./package.json');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');

module.exports = (_env, argv) => {
  let isDebug = argv.mode !== 'production';

  const config = {
    mode: !isDebug ? 'development' : 'production',
    context: path.resolve(__dirname, './src'),
    name: 'client',
    target: 'web',
    entry: {
      client: [
        './js/index.js'
      ]
    },
    output: {
      path: path.resolve(__dirname, './dist'),
      filename: isDebug ? 'js/[name].js' : 'js/[name].[fullhash:8].js'
    },
    devServer: {
      contentBase: path.join(__dirname, './dist'),
      port: 3000,
      hot: true,
      writeToDisk: true
    },
    module: {
      rules: [
        { test: /\.hbs$/, loader: "handlebars-loader" },
        {
          test: /\.(css|scss|sass)$/,
          include: [
            path.resolve(__dirname, './src/')
          ],
          use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            'sass-loader'
          ]
        },
        {
          test: /\.js$/,
          include: [
            path.resolve(__dirname, './src')
          ],
          loader: 'babel-loader',
          exclude: /node_modules/,
          options: {
            babelrc: false,
            presets: [
              [
                '@babel/preset-env',
                {
                  targets: pkg.browserslist
                }
              ]
            ]
          }
        }
      ]
    },
    plugins: [
      new webpack.LoaderOptionsPlugin({
        options: {
          handlebarsLoader: {}
        }
      }),
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        title: 'Bolerplate',
        inject: true,
        minify: false,
        scriptLoading: 'blocking',
        template: path.resolve(__dirname, './src/hbs/index.hbs')
      }),
      new HtmlWebpackPlugin({
        title: 'Bolerplate',
        inject: true,
        minify: false,
        scriptLoading: 'blocking',
        filename: 'news.html',
        template: path.resolve(__dirname, './src/hbs/news.hbs')
      }),
      new MiniCssExtractPlugin({
        filename: isDebug ? 'css/[name].css' : 'css/[name].[fullhash:5].css',
        chunkFilename: isDebug ? 'css/[name].chunk.css' : 'css/[name].[fullhash:5].chunk.css'
      })
    ],
    devtool: isDebug ? 'inline-cheap-module-source-map' : undefined
  };

  return config;
}
