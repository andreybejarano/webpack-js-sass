# Install
### clone
``` shell
$ git clone https://gitlab.com/andreybejarano/webpack-js-sass.git
```

### Install dependencies
``` shell
$ npm install
```

## Start
### Run develop mode
``` shell
$ npm run dev
```

### Build producction
``` shell
$ npm run build
```

