import '../sass/index.scss';
import { index, createCards } from "./pages/home";

const p = new Promise((resolve, reject) => {
  setTimeout(() => {
    return resolve('hola promesa');
  }, 2000);
})

async function getHelloPromise() {
  try {
    const hello =  await p;
    console.log(hello);
  } catch (error) {
    throw new Error(error);
  }
}

getHelloPromise();
index();
createCards('sasasas', 'content');

