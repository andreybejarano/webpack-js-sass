async function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    const error = {
      status: response.status,
      statusText: response.statusText,
      response: await response.json()
    };
    if (process.env.NODE_ENV === 'production') {
      _LTracker.push({
        category: 'Service Error',
        urlApi: response.url,
        userAgent: window.navigator.userAgent,
        username: localStorage.getItem('qubit_username'),
        userId: localStorage.getItem('qubit_userId'),
        browserFullname: navigator.browser.fullName,
        message: error.response.message,
        data: error.response.data,
        code: error.response.code,
        status: error.status
      });
    }
    throw error;
  }
}

function parseJSON(response) {
  return response.json();
}

export default (url, options = {}) => {
  options.credentials = 'include';
  options.mode = 'cors';
  options.cache = 'default';
  options.body = JSON.stringify(options.body);
  options.headers = {
    ...options.headers,
    'Content-Type': 'application/json'
  };

  return new Promise((resolve, reject) => {
    fetch(url, options)
      .then(checkStatus)
      .then(parseJSON)
      .then(response => resolve(response))
      .catch((error) => reject(error));
  });
};
